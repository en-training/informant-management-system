package com.example.oop;

public class MotorCycle extends Transport {
  private String make;
  public MotorCycle(String make) {
    super(1900);
    this.make = make;
  }

  @Override
  public int getNoOfWheels() {
    if(Constatnts.DEFAULT_NO_WHEELS > 2)
     return 2;
    return Constatnts.DEFAULT_NO_WHEELS;
  }

  @Override
  public int calculateAge() {
    return 0;
  }
}
