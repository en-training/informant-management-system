package com.example.oop;

public final class Car extends Transport implements ITransport {

  private String model;
  private String make;

  private final String color;

  public Car() {
    this(null, null,1900, "Grey");
  }

  public Car(String model, String make) {
    super(1900);
    this.model = model;
    this.make = make;
    this.color = "Grey";
  }

  public Car(String model, String make, int year, String color) {
    super(year);
    this.model = model;
    this.make = make;
    this.color = color;
  }

  public String getMake() {
    return make;
  }

  public void setMake(String make) {
    this.make = make;
  }

  @Override
  public int calculateAge() {
    return 0;
  }

  @Override
  public String testInterfaceImpl(String name) {
    return null;
  }

/*  @Override
  public String toString() {
    return "Car{" +
        "model='" + model + '\'' +
        ", make='" + make + '\'' +
        '}';
  }*/
}
