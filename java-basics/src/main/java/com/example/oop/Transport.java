package com.example.oop;

public abstract class Transport {

  private int year;
  private int noOfWheels;

  public Transport(int year) {
    this(year, Constatnts.DEFAULT_NO_WHEELS);
  }

  public Transport(int year, int noOfWheels) {
    this.year = year;
    this.noOfWheels = noOfWheels;
  }
  public int getManufacturedYear() {
    return year;
  }

  public  int getNoOfWheels() {
    return Constatnts.DEFAULT_NO_WHEELS;
  }

  public abstract int calculateAge();
}
