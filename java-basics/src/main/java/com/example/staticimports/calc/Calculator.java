package com.example.staticimports.calc;

public class Calculator {

  public static Integer add(Integer num1, Integer num2) {
  //  return num1 + num2;
    return Math.addExact(num1, num2);
  }

  /**
   * To add three number
   * @param num1
   * @param num2
   * @param num3
   * @return
   */
  public static int xyz(int num1, int num2, int num3) {
    // Single line comment

    /*
    Multi line
    comment
     */
      return num1 + num2 + num3;
  }
}
