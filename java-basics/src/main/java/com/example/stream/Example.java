package com.example.stream;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Example {

  public static void streamExample() {
    List<String> names = Arrays.asList("jane", "joe", "dow");

    List<String> uppercaseNames =  names.stream()
        .filter(name -> name.startsWith("j"))
        .map(name -> name.toUpperCase())
        .collect(Collectors.toList());

    uppercaseNames.forEach(name -> System.out.println(name));
  }


  public static void main(String[] args) {
    Example.streamExample();
  }

}
