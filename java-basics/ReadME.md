# Java Basics
Java is an Object oriented programing language.  
Anything that we are trying to represent in java (except for primitive data types) will be defined as a class.  

Java is a case-sensitive language. We need to consider this when we are naming variables, classes and methods.
```java
class Example {
  // These two are different variables
  String make;
  String MAKE;
}

```
#### Java Commands
javac - is used to convert the source file to a bytecode class file
java - is used to execute the bytecode file

    javac <path to source file>FileName.java - this will create FileName.class file in the same package as the source
    
    java <path to source file>FileName - will execute the bytecode class file

**NOTE**: when we use IDEs they are handling this calls for us

## Class
A class is a definition or blueprint of an object.
It has a name and an optional access modifier
if the access modifier is omitted - it will be default and that limits its visibility within the package that it belongs to.

The file name must be the same as the class name.

### Naming Convention 
A class name must start with Uppercase letter. If it has many words, convert the first letter of each word to uppercase

### syntax
Optional access-modifier followed by a **class** keyword and Name of the class

```java
//With access modifier
public class Car {
  //...
}

//Without access modifier
class CarMaker {
  //...
}

```

## Package
A package is a place that is used to store related classes in a given project or library.  
It is like a directory in file system. It is useful to differentiate classes.

### Naming Convention
use all lowercase letters separated by a dot (.).
If it has multiple words, merge all words into single word  without space

### syntax

```java
package com.example.models;

public class Car {
}

```

```java
//multiple words example
package com.example.valueobject;

public class Car {
}
```
## method, constructor and attribute
A method is a block of code that has a name and can perform specific task.  
it has an optional access modifier, return type, name and optional arguments.

    public - visible for all
    private - visible within the class only
    protected - visible within the pacakage and the class that derived from
    default - like protected it is visible in the same package but ot in derived classes

methods are access using a dot(.) operator when we call them outside the class

Constructor is a special type of method that uses the class name has its name and doesn't have return type.  
It is used to create or instantiate objects of the class type.

constructors are called the ***new*** operator

    Some key words that are used with constructor
    new
    this
    super

```java
package com.example.models;

public class CarMaker {
    private Car car;
    
    public CarMaker() {
    // No argument constructor
    }
    
    //constructor with argument
    public CarMaker(Car car) {
        this.car = car;
    }
    
    public Car getCar() {
       return this.car;
    }
    
    public void setCar(Car car) {
       this.car = car;
    }
    
    public Car fetchCar(Long carId) {
      Car car;
      //do something here to get car detail
      return car;
    }
    
    public void builtCar() {
        //..
    }
}
```
### import statement
Import statement is useful to import other visible classes into the class.

by default ***java.lang*** package is imported for all classes
```java
package com.example;

import com.test.CarMaker;
import com.test.*; //Import all classes from com.test package
import static com.test.CarMaker.*; // imports all static methods and constants from carMaker class
import static com.test.CarMaker.DEFAULT_MAKER;


public class Car {
  private carmaker carMaker;
}
```

## OOP in Java

### Object
It is an instance of a class. 
Mainly it is created or instantiated using a constructor

```java
public class Example {
  public void method() {
    Example example = new Example();
  }
}
```

### Encapsulation
The process that bundles the state and behaviour of an object in a class.
protecting the behaviours from external access and providing an accessor methods

```java
    public class Car {
      private String model;
      private String make;
      
      public Car() {
        
      }
      
      public String getModel() {
        return model;
      }
      
      public void setModel(String model) {
        this.model = model;
      }
    }
```
### Inheritance
It's useful for reuse existing or common functionalities.  
Java doesn't support multiple inheritance. A class can extend only from one class.

All Classes in java by default extends ***java.lang.Object*** class.  
This will be ignored when a class extends another class

```java

public class Transport {
}

public class Car extends Transport {
}

```

### polymorphism
Polymorphism means having many forms. 
We have two types of polymorphism

    Method overloading
        having more than one method with the same name in a given class
    Method Overriding
        Overriding a method defined in the parent class in the child class


```java

public class Transport {

}

public class Car extends Transport {
}

```

### Abstraction
Hiding the implementation details.
The client or consumer now only the definition.

We can achieve this using
    
    Abstract class
        A class that ca define an abstract methods (methods wit out bodies) and also a concrete method.
        We can't instantiate an object from abstract class
    Interface 
        It special type of class that is used to declare methods with out bodies.

```java

public abstract class AbstractTransport {
    private TransportType transportType;
    
    public abstract TransportType getTransportType(Transport transport);
}

public class Car extends AbstractTransport {

    @Override
    public TransportType  getTransportType(Transport transport) {
      //.....
    }
}

public interface ITransport {
    TransportType getTransportType(Transport transport);
}

public class Car implements ITransport {

    @Override
    public TransportType  getTransportType(Transport transport) {
      //.....
    }
    
}

```

### Access Modifiers
For class

    public
    default - we not mentioned

For methods, attributes and constructors

    public
    private
    protected
    default

public - visible anywhere
private - visible within the class only
protected - visible within the package and in the derived classes
default - like protected it is visible in the same package but not in derived classes


### Non-Access Modifiers
For class

    final
    abstract - we not mentioned

For methods, attributes

    final - when we want to block ovverridig a method or not to be modified for attributes
    static - it is accessable using the class not objects
    abstract - it is used only for method declaration in abstract class



### Enums
A special type of class that is used to store predefined constants

```java
public enum Day {
  MONDAY,
  TUESDAY,
  WEDNESDAY,
  THURSDAY,
  FRIDAY, 
  SATURDAY,
  SUNDAY
}

public class Example {
  Day day = Day.FRIDAY;
}

```


### Wrapper classes
Java is not fully OO programming language due to the primitive data types that we have

    byte, short, int, long, float, double, char and boolean
    
    Wrapper classes
    Byte, Short, Integer, Long, Float, Double, Character and Boolean

In some areas primitive data types are not useful:
    
    Like in ORM - Entity class - for nullable attribute
    We can't use them in Collections (ArrayList,....)


```java
public class Example {
  boolean isActive; // default to false
  Boolean status; // default to null
}
```

### Inner class
Java allowed us to have single class per file. But we can define a nested class(a class within another class);

Unlike the base class, inner classes can have private and protected access modifiers.
We can use nested class to group related functionalities with a given class.

A nested class ca access the private members of the base class.

We can use Nexted class to implementing Builder for the class

```java

public class Car {
  
  private String model;
  private String make;
  
  private Car(Builder builder) {
    model = builder.model;
    make = builder.make;
  }

  public static Builder builder() {
    return new Builder();
  }
  
  private final class Builder {
    private String model;
    private String make;
    
    private Builder() {
      
    }
    
    public Builder model(String val) {
      model = val;
      return this;
    }

    public Builder make(String val) {
      make = val;
      return this;
    }

    public Car build() {
      return new Car(this);
    }
  }
}
```

### Java stream

Unlike collections (List, Set, ..) stream is not a data structure.
Stream is a representation of a sequence of objects from a given source.
The source can be a Collection(list, set) or I/O

To generate stream from the source we can use:

    stream() - to get sequential objects as a stream 
    parallelStream() - to generate a parrallel stream from the source

It has aggregate functions that returns a stream: 
    
    filter - we can use it to filter out some elements from the stream
    map - to map it to other object or modifiy some fileds in the resulting stream 

this functions can be called in pipelined way

Collectors
    
    Its useful to collect the final result of the tream operations.

```java
import java.util.Arrays;
import java.util.stream.Collectors;

public class Example {
  public void streamExample() {
    List<String> names = Arrays.asList("jane", "joe", "dow");

    List<String> uppercaseNames =  names.stream()
        .map(name -> name.toUpperCase())
        .collect(Collectors.toList());

    uppercaseNames.forEach(name -> System.out::println);
  }
}

```