DROP TYPE IF EXISTS notification_status;
CREATE TYPE notification_status AS ENUM ('READ','UN_READ');

DROP TABLE IF EXISTS notification CASCADE;

CREATE TABLE notification(
      id            uuid NOT NULL,
      title         character varying COLLATE pg_catalog."default" NOT NULL,
      message       character varying COLLATE pg_catalog."default" NOT NULL,
      status        notification_status NOT NULL,

      created_by    character varying COLLATE pg_catalog."default",
      created_date  TIMESTAMP not null,
      modified_by   character varying COLLATE pg_catalog."default",
      modified_date TIMESTAMP,

      CONSTRAINT notification_pkey PRIMARY KEY (id)
);