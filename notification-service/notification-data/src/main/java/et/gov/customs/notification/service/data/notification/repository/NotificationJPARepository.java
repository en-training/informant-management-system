package et.gov.customs.notification.service.data.notification.repository;

import et.gov.customs.notification.service.data.notification.entity.NotificationEntity;
import et.gov.customs.notification.service.domain.valueobject.NotificationStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface NotificationJPARepository extends JpaRepository<NotificationEntity, UUID> {

  @Query("select  count(n.id) from NotificationEntity n where n.status = :status")
  Integer countNotificationEntitiesByStatus(@Param("status")NotificationStatus status);

}
