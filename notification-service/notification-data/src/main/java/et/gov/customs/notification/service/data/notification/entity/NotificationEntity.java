package et.gov.customs.notification.service.data.notification.entity;

import et.gov.customs.data.entity.AuditableEntity;
import et.gov.customs.notification.service.domain.valueobject.NotificationStatus;
import lombok.*;

import javax.persistence.*;
import java.util.UUID;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "notification")
public class NotificationEntity extends AuditableEntity {

  @Id
  private UUID id;
  private String title;
  private String message;

  @Enumerated(value = EnumType.STRING)
  private NotificationStatus status;
}
