package et.gov.customs.notification.service.data.notification.mapper;

import et.gov.customs.notification.service.data.notification.entity.NotificationEntity;
import et.gov.customs.notification.service.domain.entity.Notification;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

@Mapper(
    componentModel = "spring",
    unmappedTargetPolicy = ReportingPolicy.ERROR
)
public interface NotificationDataAccessMapper {

    @Mapping(target = "id", source = "notification.id.value")
    NotificationEntity notificationToNotificationEntity(Notification notification);

    @Mapping(target = "notificationId.value", source = "notificationEntity.id")
    Notification notificationEntityToNotification(NotificationEntity notificationEntity);
}
