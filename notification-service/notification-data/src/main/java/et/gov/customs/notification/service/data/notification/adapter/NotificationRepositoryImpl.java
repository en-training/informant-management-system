package et.gov.customs.notification.service.data.notification.adapter;

import et.gov.customs.notification.service.data.notification.entity.NotificationEntity;
import et.gov.customs.notification.service.data.notification.mapper.NotificationDataAccessMapper;
import et.gov.customs.notification.service.data.notification.repository.NotificationJPARepository;
import et.gov.customs.notification.service.domain.entity.Notification;
import et.gov.customs.notification.service.domain.ports.output.NotificationRepository;
import et.gov.customs.notification.service.domain.valueobject.NotificationStatus;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class NotificationRepositoryImpl implements NotificationRepository {

    private final NotificationDataAccessMapper notificationDataAccessMapper;
    private final NotificationJPARepository notificationJPARepository;

    public NotificationRepositoryImpl(NotificationDataAccessMapper notificationDataAccessMapper,
                                      NotificationJPARepository notificationJPARepository) {
        this.notificationDataAccessMapper = notificationDataAccessMapper;
        this.notificationJPARepository = notificationJPARepository;

    }
    @Override
    public Notification save(Notification notification) {
        NotificationEntity entity = notificationDataAccessMapper.notificationToNotificationEntity(notification);
        entity = notificationJPARepository.save(entity);

        return notificationDataAccessMapper.notificationEntityToNotification(entity);
    }

    @Override
    public List<Notification> findAll() {
        List<NotificationEntity> notificationEntities = notificationJPARepository.findAll();

        List<Notification> notifications = new ArrayList<>();

        if(CollectionUtils.isEmpty(notificationEntities)) {
            return notifications;
        }

        notifications = notificationEntities.stream()
            .map(notificationEntity -> notificationDataAccessMapper.notificationEntityToNotification(notificationEntity))
            .collect(Collectors.toList());
        return notifications;
    }

    @Override
    public Notification markStatus(Notification notification) {

        Optional<NotificationEntity> optionalNotification = notificationJPARepository.findById(notification.getId().getValue());

        NotificationEntity notificationEntity = optionalNotification.isPresent() ? optionalNotification.get() : null;

        if(notificationEntity == null) {
            return null;
        }
        notificationEntity.setStatus(notification.getStatus());

        NotificationEntity result = notificationJPARepository.save(notificationEntity);

        return notificationDataAccessMapper.notificationEntityToNotification(result);
    }

    @Override
    public Integer countUnreadNotifications() {
        return notificationJPARepository.countNotificationEntitiesByStatus(NotificationStatus.UN_READ);
    }
}
