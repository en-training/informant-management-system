package et.gov.customs.notification.service.domain.mapper;

import et.gov.customs.notification.service.domain.dto.create.CreateNotificationCommand;
import et.gov.customs.notification.service.domain.dto.create.CreateNotificationResponse;
import et.gov.customs.notification.service.domain.dto.create.MarkNotificationCommand;
import et.gov.customs.notification.service.domain.dto.create.MarkNotificationResponse;
import et.gov.customs.notification.service.domain.dto.search.NotificationResponse;
import et.gov.customs.notification.service.domain.dto.search.SearchNotificationResponse;
import et.gov.customs.notification.service.domain.entity.Notification;
import et.gov.customs.notification.service.domain.valueobject.NotificationId;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class NotificationDataMapper {

    public Notification createNotificationCommandToNotification(CreateNotificationCommand command) {
        return Notification.builder()
                .title(command.getTitle())
                .message(command.getMessage())
                .build();
    }

    public CreateNotificationResponse notificationToCreateNotificationResponse(Notification notification, String message) {
        return CreateNotificationResponse.builder()
                .notificationId(notification.getId().getValue())
                //.title(notification.getTitle())
                .message(message)

                .build();

    }

    public SearchNotificationResponse notificationToCreateNotificationResponse(List<Notification> notifications) {
        return SearchNotificationResponse.builder()
            .notificationResponses(notifications.stream()
                .map(notification -> notificationToNotificationResponse(notification)).collect(Collectors.toList()))
            .build();
    }

    private NotificationResponse notificationToNotificationResponse(Notification notification) {
        return NotificationResponse.builder()
            .notificationId(notification.getId().getValue())
            .title(notification.getTitle())
            .message(notification.getMessage())
            .status(notification.getStatus())
            .build();
    }


    public Notification markNotificationCommandToNotification(MarkNotificationCommand markNotificationCommand) {
        return Notification.builder()
                .notificationId(new NotificationId(markNotificationCommand.getNotificationId()))
                .status(markNotificationCommand.getMarkTo())
                .build();
    }

    public MarkNotificationResponse notificationToMarkNotificationResponse(Notification notification,
                                                                           String notificationMarkedMessage) {

        return MarkNotificationResponse.builder()
                .notificationId(notification.getId().getValue())
                .status(notification.getStatus())
                .message(notificationMarkedMessage)
                .build();
    }
}
