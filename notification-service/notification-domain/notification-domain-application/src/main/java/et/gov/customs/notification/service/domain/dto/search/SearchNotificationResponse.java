package et.gov.customs.notification.service.domain.dto.search;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import java.util.List;
import java.util.UUID;

@Getter
@Builder
@AllArgsConstructor
public class SearchNotificationResponse {
  private final List<NotificationResponse> notificationResponses;
}
