package et.gov.customs.notification.service.domain;

import et.gov.customs.notification.service.domain.dto.create.CreateNotificationCommand;
import et.gov.customs.notification.service.domain.dto.create.CreateNotificationResponse;
import et.gov.customs.notification.service.domain.event.NotificationCreatedEvent;
import et.gov.customs.notification.service.domain.mapper.NotificationDataMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class CreateNotificationCommandHandler {
    
    private final CreateNotificationCommandHelper createNotificationCommandHelper;
    private final NotificationDataMapper notificationDataMapper;
    
    public CreateNotificationCommandHandler(CreateNotificationCommandHelper createNotificationCommandHelper,
                                            NotificationDataMapper notificationDataMapper) {
        this.createNotificationCommandHelper = createNotificationCommandHelper;
        this.notificationDataMapper = notificationDataMapper;
    }
    
    

    public CreateNotificationResponse handleCreateNotification(CreateNotificationCommand command) {

        NotificationCreatedEvent notificationCreationEvent = this.createNotificationCommandHelper.persistNotification(command);
        log.info("Notification is created with id {}",notificationCreationEvent.getNotification().getId().getValue());
        return notificationDataMapper.notificationToCreateNotificationResponse(
                notificationCreationEvent.getNotification(),
                "Successfully Notification created");
    }
}
