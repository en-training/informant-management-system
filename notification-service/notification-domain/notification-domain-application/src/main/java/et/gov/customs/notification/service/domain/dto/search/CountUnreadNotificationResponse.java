package et.gov.customs.notification.service.domain.dto.search;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import java.util.List;

@Getter
@Builder
@AllArgsConstructor
public class CountUnreadNotificationResponse {
  private final Integer noOfUnreadNotification;
}
