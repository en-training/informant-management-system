package et.gov.customs.notification.service.domain;

import et.gov.customs.notification.service.domain.dto.create.CreateNotificationCommand;
import et.gov.customs.notification.service.domain.dto.create.CreateNotificationResponse;
import et.gov.customs.notification.service.domain.dto.create.MarkNotificationCommand;
import et.gov.customs.notification.service.domain.dto.create.MarkNotificationResponse;
import et.gov.customs.notification.service.domain.dto.search.CountUnreadNotificationResponse;
import et.gov.customs.notification.service.domain.dto.search.SearchNotificationResponse;
import et.gov.customs.notification.service.domain.ports.input.service.NotificationApplicationService;
import org.springframework.stereotype.Service;

@Service
public class NotificationApplicationServiceImpl implements NotificationApplicationService  {

    private final CreateNotificationCommandHandler createNotificationCommandHandler;
    private final SearchNotificationCommandHandler searchNotificationCommandHandler;
    private final MarkNotificationCommandHandler markNotificationCommandHandler;

    public NotificationApplicationServiceImpl(CreateNotificationCommandHandler createNotificationCommandHandler,
                                              SearchNotificationCommandHandler searchNotificationCommandHandler,
                                              MarkNotificationCommandHandler markNotificationCommandHandler) {
        this.createNotificationCommandHandler = createNotificationCommandHandler;
        this.searchNotificationCommandHandler = searchNotificationCommandHandler;
        this.markNotificationCommandHandler = markNotificationCommandHandler;
    }
    @Override
    public CreateNotificationResponse createNotification(CreateNotificationCommand command) {
        return createNotificationCommandHandler.handleCreateNotification(command);
    }

    @Override
    public SearchNotificationResponse fetchNotifications() {
        return searchNotificationCommandHandler.searchNotifications();
    }

    @Override
    public MarkNotificationResponse markNotification(MarkNotificationCommand markNotificationCommand) {
        return markNotificationCommandHandler.handleCreateNotification(markNotificationCommand);
    }

    @Override
    public CountUnreadNotificationResponse countUnreadNotifications() {
        return searchNotificationCommandHandler.countUnreadNotifications();
    }
}
