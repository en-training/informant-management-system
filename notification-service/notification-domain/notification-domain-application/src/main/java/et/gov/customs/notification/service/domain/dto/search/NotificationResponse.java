package et.gov.customs.notification.service.domain.dto.search;

import et.gov.customs.notification.service.domain.valueobject.NotificationStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import java.util.UUID;

@Getter
@Builder
@AllArgsConstructor
public class NotificationResponse {
  private UUID notificationId;
  private String title;
  private String message;
  private NotificationStatus status;
}
