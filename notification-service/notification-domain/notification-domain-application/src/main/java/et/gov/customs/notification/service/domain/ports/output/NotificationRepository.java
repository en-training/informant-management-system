package et.gov.customs.notification.service.domain.ports.output;

import et.gov.customs.notification.service.domain.entity.Notification;

import java.util.List;

public interface NotificationRepository {

    Notification save(Notification notification);

    List<Notification> findAll();

    Notification markStatus(Notification notification);

  Integer countUnreadNotifications();
}
