package et.gov.customs.notification.service.domain;

import et.gov.customs.notification.service.domain.dto.create.MarkNotificationCommand;
import et.gov.customs.notification.service.domain.entity.Notification;
import et.gov.customs.notification.service.domain.event.NotificationMarkedEvent;
import et.gov.customs.notification.service.domain.exception.NotificationDomainException;
import et.gov.customs.notification.service.domain.mapper.NotificationDataMapper;
import et.gov.customs.notification.service.domain.ports.output.NotificationRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;


@Slf4j
@Component
public class MarkNotificationCommandHelper {

    private final NotificationDataMapper notificationDataMapper;
    private final NotificationDomainService notificationDomainService;
    private final NotificationRepository notificationRepository;

    public MarkNotificationCommandHelper(NotificationDataMapper notificationDataMapper,
                                         NotificationDomainService notificationDomainService,
                                         NotificationRepository notificationRepository) {
        this.notificationDataMapper = notificationDataMapper;
        this.notificationDomainService = notificationDomainService;
        this.notificationRepository = notificationRepository;
    }

    @Transactional
    public NotificationMarkedEvent updateNotification(MarkNotificationCommand markNotificationCommand) {

        Notification notification = notificationDataMapper.markNotificationCommandToNotification(markNotificationCommand);

        NotificationMarkedEvent notificationMarkedEvent = notificationDomainService.validateToMarkStatus(notification);

        markNotification(notificationMarkedEvent.getNotification());

        return notificationMarkedEvent;
    }

    private void markNotification(Notification notification) {
        Notification result = notificationRepository.markStatus(notification);

        if(result == null) {
            log.error("Could not mark Notification");
            throw new NotificationDomainException("Could not mark Notification");
        }

        log.info("Notification is marked to {} with id {}", result.getStatus(), result.getId().getValue());

    }
}
