package et.gov.customs.notification.service.domain;

import et.gov.customs.notification.service.domain.entity.Notification;
import et.gov.customs.notification.service.domain.event.NotificationCreatedEvent;
import et.gov.customs.notification.service.domain.event.NotificationMarkedEvent;

public interface NotificationDomainService {

    NotificationCreatedEvent validateAndInitializeNotification(Notification notification);

    NotificationMarkedEvent validateToMarkStatus(Notification notification);
}
