package et.gov.customs.notification.service.domain.event;

import et.gov.customs.notification.service.domain.entity.Notification;

public class NotificationMarkedEvent extends NotificationEvent {
    public NotificationMarkedEvent(Notification notification) {
        super(notification);
    }

}
