package et.gov.customs.notification.service.domain.entity;

import et.gov.customs.domain.entity.AggregateRoot;
import et.gov.customs.notification.service.domain.exception.NotificationDomainException;
import et.gov.customs.notification.service.domain.valueobject.NotificationId;
import et.gov.customs.notification.service.domain.valueobject.NotificationStatus;

import java.util.UUID;

public class Notification extends AggregateRoot<NotificationId> {

    private final String title;
    private final String message;
    private NotificationStatus status;


    private Notification(Builder builder) {
        super.setId(builder.notificationId);
        message = builder.message;
        title = builder.title;
        status = builder.status;
    }

    public void validateInitialNotification() {
        if(getId() != null) {
            throw new NotificationDomainException("Notification is not in correct state for initialization");
        }
    }

    public void validateForMarkStatus() {
        if(getId() == null) {
            throw new NotificationDomainException("Notification is not in correct state for marking status");
        }

        if(status == null) {
            throw new NotificationDomainException("Notification is not in correct state for marking status");
        }
    }

    public void initializeNotification() {
        setId(new NotificationId(UUID.randomUUID()));
        this.status = NotificationStatus.UN_READ;
    }

    public String getTitle() {
        return title;
    }

    public String getMessage() {
        return message;
    }

    public NotificationStatus getStatus() {
        return status;
    }

    public static Builder builder() {
        Builder builder = new Builder();
        return builder;
    }

    public static final class Builder {

        private NotificationId notificationId;
        private String title;
        private String message;

        private NotificationStatus status;

        private Builder() {

        }

        public Builder notificationId(NotificationId val) {
            notificationId = val;
            return this;
        }

        public Builder title(String val) {
            this.title = val;
            return this;
        }

        public Builder message(String val) {
            this.message = val;
            return this;
        }

        public Builder status(NotificationStatus val) {
            this.status = val;
            return this;
        }


        public Notification build() {
            return new Notification(this);
        }

    }
}
