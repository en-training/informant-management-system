package et.gov.customs.notification.service.domain.exception;

import et.gov.customs.domain.exception.DomainException;

public class NotificationDomainException extends DomainException {
    public NotificationDomainException(String message) {
        super(message);
    }


    public NotificationDomainException(String message, Throwable throwable) {
        super(message, throwable);
    }

}
