package et.gov.customs.notification.service.domain.valueobject;

public enum NotificationStatus {
    READ,
    UN_READ
}
