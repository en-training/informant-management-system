package et.gov.customs.notification.service.domain;

import et.gov.customs.notification.service.domain.entity.Notification;
import et.gov.customs.notification.service.domain.event.NotificationCreatedEvent;
import et.gov.customs.notification.service.domain.event.NotificationMarkedEvent;

import java.time.ZoneId;
import java.time.ZonedDateTime;

public class NotificationDomainServiceImpl implements NotificationDomainService{

    @Override
    public NotificationCreatedEvent validateAndInitializeNotification(Notification notification) {
        notification.validateInitialNotification();
        notification.initializeNotification();
        return new NotificationCreatedEvent(notification, ZonedDateTime.now(ZoneId.of("UTC")));
    }

    @Override
    public NotificationMarkedEvent validateToMarkStatus(Notification notification) {
        notification.validateForMarkStatus();
        return new NotificationMarkedEvent(notification);
    }
}
