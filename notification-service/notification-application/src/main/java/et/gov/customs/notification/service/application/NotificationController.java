package et.gov.customs.notification.service.application;

import et.gov.customs.application.api.Response;
import et.gov.customs.notification.service.domain.dto.create.CreateNotificationCommand;
import et.gov.customs.notification.service.domain.dto.create.CreateNotificationResponse;
import et.gov.customs.notification.service.domain.dto.create.MarkNotificationCommand;
import et.gov.customs.notification.service.domain.dto.create.MarkNotificationResponse;
import et.gov.customs.notification.service.domain.dto.search.CountUnreadNotificationResponse;
import et.gov.customs.notification.service.domain.dto.search.SearchNotificationResponse;
import et.gov.customs.notification.service.domain.ports.input.service.NotificationApplicationService;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("notifications")
public class NotificationController {

  private final NotificationApplicationService notificationApplicationService;

  public NotificationController(NotificationApplicationService notificationApplicationService) {
    this.notificationApplicationService = notificationApplicationService;
  }

  @GetMapping()
  public Response<SearchNotificationResponse> fetchNotifications() {
    SearchNotificationResponse response =  this.notificationApplicationService.fetchNotifications();
    return Response.<SearchNotificationResponse>builder().data(response).build();
  }

  @GetMapping("count-un-read-notifications")
  public Response<CountUnreadNotificationResponse> countUnreadNotifications() {
    CountUnreadNotificationResponse response =  this.notificationApplicationService.countUnreadNotifications();
    return Response.<CountUnreadNotificationResponse>builder().data(response).build();
  }

  @PostMapping()
  public Response<CreateNotificationResponse> createNotification(@RequestBody CreateNotificationCommand createNotificationCommand) {
    CreateNotificationResponse response = notificationApplicationService.createNotification(createNotificationCommand);
    return Response.<CreateNotificationResponse>builder().data(response).build();
  }

  @PutMapping("mark-status")
  public Response<MarkNotificationResponse> markNotification(@RequestBody MarkNotificationCommand markNotificationCommand) {
    MarkNotificationResponse response = notificationApplicationService.markNotification(markNotificationCommand);
    return Response.<MarkNotificationResponse>builder().data(response).build();
  }
}
