package et.gov.customs.informant.service.application.controller;

import et.gov.customs.application.api.Response;
import et.gov.customs.informant.service.domain.dto.create.CreateInformantCommand;
import et.gov.customs.informant.service.domain.dto.create.CreateInformantResponse;
import et.gov.customs.informant.service.domain.dto.search.SearchInformantResponse;
import et.gov.customs.informant.service.domain.ports.input.service.InformantApplicationService;
import org.springframework.web.bind.annotation.*;

@RestController()
@RequestMapping("informants")
public class InformantController {

  private final InformantApplicationService informantApplicationService;

  public InformantController(InformantApplicationService informantApplicationService) {
    this.informantApplicationService = informantApplicationService;
  }

  @GetMapping()
  public Response<SearchInformantResponse> searchInformants() {
    SearchInformantResponse searchInformantResponse = informantApplicationService.searchInformants();
    return new Response<>(searchInformantResponse);
  }

  @PostMapping
  public Response<CreateInformantResponse> recordInformant(@RequestBody CreateInformantCommand createInformantCommand) {
    CreateInformantResponse response = informantApplicationService.recordInformant(createInformantCommand);
    return new Response<>(response);
  }
}
