
DROP TABLE IF EXISTS intel_info CASCADE;

CREATE TABLE intel_info(
                           id            uuid NOT NULL,
                           description    character varying COLLATE pg_catalog."default" NOT NULL,

                           created_by    character varying COLLATE pg_catalog."default",
                           created_date  TIMESTAMP not null,
                           modified_by   character varying COLLATE pg_catalog."default",
                           modified_date TIMESTAMP,

                           CONSTRAINT intel_info_pkey PRIMARY KEY (id)
);

ALTER TABLE informant ADD intel_info_id uuid NULL;

ALTER TABLE informant
    ADD CONSTRAINT "FK_INTEL_INFO" FOREIGN KEY (intel_info_id)
        REFERENCES intel_info (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE CASCADE
    NOT VALID;
