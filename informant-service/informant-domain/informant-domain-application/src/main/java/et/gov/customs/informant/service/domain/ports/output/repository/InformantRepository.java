package et.gov.customs.informant.service.domain.ports.output.repository;

import et.gov.customs.informant.service.domain.entity.Informant;

import java.util.List;

public interface InformantRepository {

  Informant save(Informant informant);

  List<Informant> findAll();
}
