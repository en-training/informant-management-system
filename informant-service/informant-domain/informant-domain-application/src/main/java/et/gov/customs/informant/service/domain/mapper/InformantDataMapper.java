package et.gov.customs.informant.service.domain.mapper;

import et.gov.customs.informant.service.domain.dto.create.CreateInformantCommand;
import et.gov.customs.informant.service.domain.dto.create.CreateInformantResponse;
import et.gov.customs.informant.service.domain.entity.Informant;
import et.gov.customs.informant.service.domain.entity.IntelInfo;
import org.springframework.stereotype.Component;

@Component
public class InformantDataMapper {

  public Informant createInformantCommandTInformant(CreateInformantCommand createInformantCommand) {
    return Informant.builder()
        .firstName(createInformantCommand.getFirstName())
        .intelInfo(IntelInfo.builder()
            .description(createInformantCommand.getIntelInfoDescription())
            .build()
        ).build();
  }

  public CreateInformantResponse informantToCreateInformantResponse(Informant informant, String informantCreatedSuccessfully) {
    return CreateInformantResponse.builder()
        .informantId(informant.getId().getValue())
        .message(informantCreatedSuccessfully)
        .build();
  }
}