package et.gov.customs.informant.service.domain.ports.input.service;

import et.gov.customs.informant.service.domain.dto.create.CreateInformantCommand;
import et.gov.customs.informant.service.domain.dto.create.CreateInformantResponse;
import et.gov.customs.informant.service.domain.dto.search.SearchInformantResponse;

import javax.validation.Valid;

public interface InformantApplicationService {

  CreateInformantResponse recordInformant(@Valid CreateInformantCommand createInformantCommand);

  SearchInformantResponse searchInformants();
}
