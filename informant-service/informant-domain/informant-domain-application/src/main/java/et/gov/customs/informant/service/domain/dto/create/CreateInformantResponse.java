package et.gov.customs.informant.service.domain.dto.create;

import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CreateInformantResponse {
  private UUID informantId;
  private String message;
}
