package et.gov.customs.informant.service.domain.valueobject;

import et.gov.customs.domain.valueObject.BaseId;

import java.util.UUID;

public class IntelInfoId extends BaseId<UUID> {
  public IntelInfoId(UUID value) {
    super(value);
  }
}