package et.gov.customs.informant.service.domain.exception;

import et.gov.customs.domain.exception.DomainException;

public class InformantDomainException extends DomainException {
  public InformantDomainException(String message) {
    super(message);
  }

  public InformantDomainException(String message, Throwable cause) {
    super(message, cause);
  }
}
