package et.gov.customs.informant.service.domain;

import et.gov.customs.informant.service.domain.entity.Informant;
import et.gov.customs.informant.service.domain.event.InformantCreatedEvent;

public interface InformantDomainService {
  InformantCreatedEvent validateAndInitiateInformant(Informant informant);
}
