package et.gov.customs.informant.service.domain.entity;

import et.gov.customs.domain.entity.AggregateRoot;
import et.gov.customs.informant.service.domain.exception.InformantDomainException;
import et.gov.customs.informant.service.domain.valueobject.InformantId;
import et.gov.customs.informant.service.domain.valueobject.IntelInfoId;

import java.util.UUID;

public class Informant extends AggregateRoot<InformantId> {
  private String firstName;
  private IntelInfo intelInfo;

  private Informant(Builder builder) {
    super.setId(builder.informantId);
    firstName = builder.firstName;
    intelInfo = builder.intelInfo;
  }

  public String getFirstName() {
    return firstName;
  }

  public IntelInfo getIntelInfo() {
    return intelInfo;
  }

  public void initializeInformant() {
    setId(new InformantId(UUID.randomUUID()));
    if(intelInfo != null) {
      intelInfo.setId(new IntelInfoId(UUID.randomUUID()));
    }
  }

  public void validateInformant() {
    validateInitialInformant();
  }

  private void validateInitialInformant() {
    if (getId() != null) {
      throw new InformantDomainException(
          "Informant is not in correct state for initialization!");
    }
  }


  public static Builder builder() {
    return new Builder();
  }

  public static final class Builder {

    private InformantId informantId;
    private String firstName;
    private IntelInfo intelInfo;

    private Builder() {
    }

    public Builder informantId(InformantId val) {
      informantId = val;
      return this;
    }

    public Builder firstName(String val) {
      firstName = val;
      return this;
    }

    public Builder intelInfo(IntelInfo val) {
      intelInfo = val;
      return this;
    }

    public Informant build() {
      return new Informant(this);
    }

  }
}
