package et.gov.customs.informant.service.domain.event;


import et.gov.customs.domain.event.DomainEvent;
import et.gov.customs.informant.service.domain.entity.Informant;

import java.time.ZonedDateTime;

public abstract class InformantEvent<P> implements DomainEvent<Informant> {

    private final Informant informant;
    private final ZonedDateTime createdAt;

    public InformantEvent(Informant informant, ZonedDateTime createdAt) {
        this.informant = informant;
        this.createdAt = createdAt;
    }

    public Informant getInformant() {
        return informant;
    }

    public ZonedDateTime getCreatedAt() {
        return createdAt;
    }
}
