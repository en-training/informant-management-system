package et.gov.customs.informant.service.domain;

import et.gov.customs.informant.service.domain.entity.Informant;
import et.gov.customs.informant.service.domain.event.InformantCreatedEvent;


import java.time.ZoneId;
import java.time.ZonedDateTime;

public class InformantDomainServiceImpl implements InformantDomainService {

  public static final String UTC = "UTC";

  @Override
  public InformantCreatedEvent validateAndInitiateInformant(Informant informant) {

    informant.validateInformant();
    informant.initializeInformant();
   // log.info("Informant with id: {} is initiated", informant.getId().getValue());
    return new InformantCreatedEvent(
        informant,
        ZonedDateTime.now(ZoneId.of(UTC)));
  }
}
