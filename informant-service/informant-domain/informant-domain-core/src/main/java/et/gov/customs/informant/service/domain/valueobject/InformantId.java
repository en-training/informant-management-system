package et.gov.customs.informant.service.domain.valueobject;

import et.gov.customs.domain.valueObject.BaseId;

import java.util.UUID;

public class InformantId extends BaseId<UUID> {
  public InformantId(UUID value) {
    super(value);
  }
}
