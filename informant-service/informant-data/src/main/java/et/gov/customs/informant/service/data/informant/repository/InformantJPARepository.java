package et.gov.customs.informant.service.data.informant.repository;

import et.gov.customs.informant.service.data.informant.entity.InformantEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface InformantJPARepository extends JpaRepository<InformantEntity, UUID> {
}
