package et.gov.customs.informant.service.data.informant.entity;

import et.gov.customs.data.entity.AuditableEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.util.UUID;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "informant")
public class InformantEntity extends AuditableEntity {
  @Id
  private UUID id;
  private String firstName;

  @OneToOne(cascade = CascadeType.ALL)
  private IntelInfoEntity intelInfo;
}
