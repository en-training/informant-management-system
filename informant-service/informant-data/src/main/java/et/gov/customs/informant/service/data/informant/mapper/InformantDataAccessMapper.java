package et.gov.customs.informant.service.data.informant.mapper;

import et.gov.customs.informant.service.data.informant.entity.InformantEntity;
import et.gov.customs.informant.service.domain.entity.Informant;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring",
    unmappedTargetPolicy = ReportingPolicy.ERROR,
    uses = {
        IntelInfoDataAccessMapper.class
    }
)
public interface InformantDataAccessMapper {

  @Mapping(target = "id", source = "informant.id.value")
  @Mapping(target = "intelInfo", source = "informant.intelInfo")
  InformantEntity informantToInformantEntity(Informant informant);

  @Mapping(target = "informantId.value", source = "informantEntity.id")
  @Mapping(target = "intelInfo", source = "informantEntity.intelInfo")
  Informant informantEntityToInformant(InformantEntity informantEntity);

}
