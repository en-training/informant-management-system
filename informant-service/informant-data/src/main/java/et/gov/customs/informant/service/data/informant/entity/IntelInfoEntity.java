package et.gov.customs.informant.service.data.informant.entity;

import et.gov.customs.data.entity.AuditableEntity;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "intel_info")
public class IntelInfoEntity extends AuditableEntity {

  @Id
  private UUID id;
  private String description;
}
