

### Commands

## pods

kubectl create -f db.yml

kubectl get pods

kubectl get pods -o wide

kubectl get pods -o json

kubectl get pods -o yaml

kubectl describe pod db

## ReplicaSets

kubectl create -f go-demo-2.yml

kubectl get rs

kubectl get -f go-demo-2.yml

kubectl describe -f go-demo-2.yml

kubectl get pods --show-labels

## remove label

POD_NAME=$(kubectl get pods -o name | tail -1 | sed 's|^pod/||')
kubectl label pod $POD_NAME service- 
kubectl get pods --show-labels

## add label
kubectl label $POD_NAME service=go-demo-2



## Operating ReplicaSets

kubectl create -f go-demo-3.yml --save-config

kubectl get pods

kubectl apply -f go-demo-4.yml

kubectl get pods

POD_NAME=$(kubectl get pods -o name \
   | tail -1)

kubectl delete $POD_NAME

kubectl get pods

POD_NAME=$(kubectl get pods -o name \
    | tail -1)

kubectl label pod $POD_NAME service-

kubectl describe $POD_NAME

kubectl get pods --show-labels

kubectl label pod $POD_NAME service=go-demo-2

kubectl get pods --show-labels

k3d cluster delete mycluster --all

## Creating Services by Exposing Ports

kubectl create -f go-demo-5.yml

kubectl get -f go-demo-5.yml

kubectl expose rs go-demo-2 \
    --name=go-demo-2-svc \
    --target-port=28017 \
    --type=NodePort

 ## Creating Services through Declarative Syntax

 kubectl create -f go-demo-6.yml
 kubectl create -f go-demo-66.yml

kubectl get -f go-demo-66.yml

##kubectl port-forward service/go-demo-2 3000:28017 --address 0.0.0.0

kubectl get ep go-demo-2 -o yaml

kubectl delete -f go-demo-66.yml

kubectl delete -f go-demo-6.yml

## deployment
kubectl create -f go-demo-77.yml 

kubectl get -f go-demo-77.yml

kubectl describe -f go-demo-77.yml

kubectl get all